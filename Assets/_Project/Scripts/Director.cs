﻿using Cinemachine;
using UnityEngine;

public class Director : MonoBehaviour
{
    public CinemachineVirtualCamera TopCamera;
    public CinemachineVirtualCamera FaceCamera;
    public CinemachineVirtualCamera FollowCamera;

    private const int FOCUS_VALUE = 15;
    private const int NONFOCUS_VALUE = 10;

    public void FocusTopCamera()
    {
        TopCamera.m_Priority = FOCUS_VALUE;
        FaceCamera.m_Priority = NONFOCUS_VALUE;
        FollowCamera.m_Priority = NONFOCUS_VALUE;
    }

    public void FocusFaceCamera()
    {
        TopCamera.m_Priority = NONFOCUS_VALUE;
        FaceCamera.m_Priority = FOCUS_VALUE;
        FollowCamera.m_Priority = NONFOCUS_VALUE;
    }

    public void FocusFollowCamera()
    {
        TopCamera.m_Priority = NONFOCUS_VALUE;
        FaceCamera.m_Priority = NONFOCUS_VALUE;
        FollowCamera.m_Priority = FOCUS_VALUE;
    }
}
