﻿using UnityEngine;
using DG.Tweening;

public class FighterAnimator : MonoBehaviour
{
    public Director Director;
    public bool ShouldAnimate;

    private Animator _animator;

    public void Start()
    {
        if (!_animator) _animator = GetComponent<Animator>();

        if (ShouldAnimate) AnimateAttack();
    }

    private void AnimateAttack()
    {
        const float ATTACK_DISTANCE = 16.5f;
        const float MOVE_TIME = 2f;
        const float PUNCH_TIME = 1f;
        const float MAX_RUN_SPEED = 1f;
        const float ANIMATION_DELAY = 3f;

        const Ease MOVE_EASE = Ease.InCubic;
        const Ease ANIM_EASE = Ease.OutCubic;

        Sequence attackSequence = DOTween.Sequence();

        //Camera work
        attackSequence.AppendCallback(() => {
            Director.FocusFaceCamera();
        });
        attackSequence.AppendInterval(3f);
        attackSequence.AppendCallback(() => {
            Director.FocusFollowCamera();
        });

        //Run Towards Target
        attackSequence.AppendCallback(() => {
            _animator.SetFloat("RunSpeed", 0);
            _animator.SetTrigger("Run");
        });
        attackSequence.AppendCallback(() => {
            transform.DOMoveZ(transform.position.z + ATTACK_DISTANCE, MOVE_TIME).SetEase(MOVE_EASE);
            DOTween.To(() => _animator.GetFloat("RunSpeed"), x => _animator.SetFloat("RunSpeed", x), MAX_RUN_SPEED, MOVE_TIME)
                .SetEase(ANIM_EASE);
        });
        attackSequence.AppendInterval(MOVE_TIME);

        //Punch Target
        attackSequence.AppendCallback(() => { _animator.SetTrigger("Punch"); });
        attackSequence.AppendInterval(PUNCH_TIME);

        //Run Back
        attackSequence.AppendCallback(() => {
            _animator.SetFloat("RunSpeed", 0);
            _animator.SetTrigger("Run");
        });
        attackSequence.AppendCallback(() => {
            transform.DOMoveZ(transform.position.z - ATTACK_DISTANCE, MOVE_TIME).SetEase(MOVE_EASE);
            DOTween.To(() => _animator.GetFloat("RunSpeed"), x => _animator.SetFloat("RunSpeed", x), -MAX_RUN_SPEED, MOVE_TIME)
                .SetEase(ANIM_EASE);
        });
        attackSequence.AppendInterval(MOVE_TIME);

        //Idle
        attackSequence.AppendCallback(() => { _animator.SetTrigger("Idle"); });
        attackSequence.AppendCallback(() => {
            Director.FocusTopCamera();
        });

        //General Settings
        attackSequence.SetDelay(ANIMATION_DELAY);
        attackSequence.SetLoops(-1);
    }
}
