﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenshotManager : MonoBehaviour
{
	private void Update ()
	{
	    if (Input.GetKeyDown(KeyCode.Space))
            ScreenCapture.CaptureScreenshot(DateTime.Now.ToFileTimeUtc() + ".png");
	}
}
